import java.util.HashSet;

public class Futbolista {
	public String nombre;
	public Integer dorsal;
	private Integer goles;
	public void marcaGoles() {goles++;}
	public Integer numeroGoles() {return goles;}
}

class Portero extends Futbolista {
	public Integer tallaGuante;
}

class Equipo {
	private HashSet<Futbolista> lista = new HashSet<Futbolista>();
	public String nombre;
	public void fichar(Futbolista f) {lista.add(f);}
	public void ceder(Futbolista f) {lista.remove(f);}
}
